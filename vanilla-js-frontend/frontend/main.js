const BACKEND_URL = "http://localhost:5050/books"
const RANDOM_PICTURES = 15
async function fetchData() {
  const request = new Request(BACKEND_URL, {method: 'GET'})
  const response = await fetch(request)
  const result = await response.json()
  if(response.status === 400) {
    console.log("Error 400, request was incorrect or corrupted and the server couldn't get the information. Please reload page and retry or Contact Support if the problem was not solved.")
  } else if(response.status === 500) {
    console.log("Error: 500 - Error in the website's server")
  }
  console.log("Record was fetched successfully.")

  document.getElementById('table').innerHTML = "";

  let htmlCode = '<div class="container">';
  htmlCode = '<div class="row">';
  for (var i = 0; i < result.length; i++) {
    var random_num_book = Math.ceil(Math.random()*RANDOM_PICTURES)
    htmlCode += '<div class="col-md-3 bookdesc">';
    htmlCode += '<br><br><img src="' + random_num_book + '.jpg" width="140" height="200" class="mr-4" alt="Book Picture">';
    htmlCode += '<br>'+ "► " + result[i].title + '';
    htmlCode += '<br>'+ "► " + result[i].author + '';
    htmlCode += '<br>'+ "► " + result[i].price + '';
    htmlCode += '<br><button type"button" class="btn btn-info btn-sm" onclick=deleteData(\'' + result[i]._id + '\')>Delete Book</button>';
    htmlCode += '<br><br><button type="button" class="btn btn-info btn-sm" onclick=putData(\'' + result[i]._id + '\')>Update Book</button>';
    htmlCode += '</div>';
  }
  htmlCode += '</div> </div>';
document.getElementById('table').innerHTML += htmlCode;

}

async function postData() {
  const data = {title: '', author: '', price: 0}
  data.title = document.getElementById('title').value
  data.author = document.getElementById('author').value
  data.price = document.getElementById('price').value

  const request = new Request(BACKEND_URL, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  })

  const response = await fetch(request)
  if(response.status === 400) {
    console.log("Error 400, request was incorrect or corrupted and the server couldn't understand it. Please Check Information or Contact Support if the problem was not solved.")
  } else if(response.status === 500) {
    console.log("Error: 500 - Error in the website's server")
  }
  const result = await response.json();
  console.log("Record was inserted successfully.")
  console.log(result)

  fetchData();
}

async function deleteData(input = '') {
  const data = {input}
  const request = new Request(BACKEND_URL + '/' + input, {
    method: 'DELETE',
    headers: {'Accept' : 'application/json',
              'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  })

  const response = await fetch(request)

  if(response.status === 400) {
    console.log("Error 400, request was incorrect or corrupted and the server couldn't understand it. Please Reload Page and Retry or Contact Support if the problem was not solved.")
  } else if(response.status === 500) {
    console.log("Error: 500 - Error in the website's server")
  }
  const result = await response.json();
  console.log("Record was Deleted")
  console.log(result)

  fetchData();
}
async function putData(id = '') {
  const data = {title: '', author: '', price: 0}
  data.title = document.getElementById('title').value
  data.author = document.getElementById('author').value
  data.price = document.getElementById('price').value

  const request = new Request(BACKEND_URL + '/' + id, {
    method: 'PUT',
    headers: {'Accept' : 'application/json',
              'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  })

  const response = await fetch(request)
  if(response.status === 400) {
    console.log("Error 400, request was incorrect or corrupted and the server couldn't understand it. Please Check Information or Contact Support if the problem was not solved.")
  } else if(response.status === 500) {
    console.log("Error: 500 - Error in the website's server")
  }
  const result = await response.json();
  console.log("Record was updated successfully!")
  console.log(result)

  fetchData();
}
