const express = require('express');
const mongodb = require('mongodb');
const bodyParser = require('body-parser');
const ObjectID = require('mongodb').ObjectID;

const books = require('./apis/books');
const orders = require('./apis/orders');
const users = require('./apis/users');

const app = express();
const PORT = 5050
const DB_URI = 'mongodb://localhost:27017';

app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  next();
});

const DB_NAME = 'Book_store';

const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

app.use(bodyParser.urlencoded({ extended: false })) // allow user to send data within the URl
app.use(bodyParser.json()); // allow user to send JSON dat

//-----------------------------------------------------------------------------------------------------------
app.use('/books', books);
app.use('/orders', orders);
app.use('/users', users);
//-----------------------------------------------------------------------------------------------------------
app.listen(PORT);
console.log("Listening on port " + PORT);
