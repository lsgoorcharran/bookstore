var express = require('express')
var router = express.Router()
const mongodb = require('mongodb');
const ObjectID = require('mongodb').ObjectID;


const DB_NAME = 'Book_store';
const BOOK_COLLECTION_NAME = "Books";

const DB_URI = 'mongodb://localhost:27017';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

//GET request
router.get('/', function (req, res) {
  client.connect(function(err, connection){
    const db = connection.db(DB_NAME); // connecting to the book store database
    db.collection(BOOK_COLLECTION_NAME)
      .find({})
      .toArray(function(find_err, records){
        if (find_err)
          return res.status(500).send(find_err);

        return res.send(records);
      });

  });
})

router.post('/', function(req, res) {
  if (!req.body || req.body.length === 0)
    return res.status(400).send({"message": "record is requried"});

  // data validation
  if (!req.body.title || !req.body.author || !req.body.price)
    res.status(400).send({
      message: "Title, author, price, ... are requried"
    });

  // date is in req.body
  client.connect(function(err, connection){
    const db = connection.db(DB_NAME);

    db.collection(BOOK_COLLECTION_NAME)
      .insertOne(req.body, function(insert_error, data){
        if (insert_error)
          return res.status(500).send({message: "Record was not inserted successfully"});

        connection.close();

        return res.status(200).send({message: "Record inserted successfully"});
      });

  });

})

router.put('/:id', function(req, res){
    client.connect(function(err, connection) {
      if(err)
        return res.status(500).send({error: err})

      if(!req.body || req.body.length === 0)
        return res.status(400).send({message: "Record is required."});

      const db = connection.db(DB_NAME)
      db.collection(BOOK_COLLECTION_NAME)
        .updateOne({_id: ObjectID(req.params.id)},{$set: req.body}, function(update_err, update_data){
            if(update_err)
              return res.status(500).send({error: "Could not update book records"})
            return res.status(200).send({error: "Update was successful", data: update_data})
        })
    })
})


router.delete('/:id', function(req, res){

  client.connect(function(err, connection) {
    if(err)
      return res.status(500).send({error: err})

      const db = connection.db(DB_NAME)
      db.collection(BOOK_COLLECTION_NAME).deleteOne({"_id": ObjectID( req.params.id)}, function(err, data){
        if(err)
          return res.status(500).send({error: "Deletion was unsuccessful", data: data})
        return res.status(200).send({error: "Deletion Successful", data: data})
      })
    })
  })


module.exports = router
