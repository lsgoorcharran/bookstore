import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  private BACKEND_URL =  "http://localhost:5050/books";
  constructor(private http: HttpClient) { }

  // Retrieving ALL books from the backend through HTTP GET
  getAllBooks(): Observable<any[]>{
    return this.http.get<any[]>(this.BACKEND_URL);
  }
}
