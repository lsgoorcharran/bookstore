import { Component, OnInit } from '@angular/core';
import { BooksService } from './books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  name: string = "Leroy S. Goorcharran";
  books: any[];

  constructor(private bookService: BooksService) { }

  ngOnInit() {
    this.bookService.getAllBooks().subscribe(
      data => {
        this.books = data;
        //console.log(data)
      },
      error => {
        console.log(error);
      }
    );
  }

}
